import re
from importlib import resources

MANIFEST_RESOURCE = "manifest.txt"
PAIR_PATTERN = re.compile(r"^\s*(\w+)\s*=\s*([^\r\n]+)$")

_manifest = dict()
with resources.open_text(__package__, MANIFEST_RESOURCE) as manifest_file:
    for line in manifest_file.readlines():
        match = PAIR_PATTERN.fullmatch(line.strip())
        if match is not None:
            key = match.group(1)
            value = match.group(2)
            _manifest[key] = value


def description():
    """
    Produces a single line of text describing the contexts of the manifest.
    :return: manifest description
    """
    description = ""
    if "name" in _manifest:
        description += f"{_manifest['name']} "
    if "build" in _manifest:
        description += f"build {_manifest['build']} "
    if "timestamp" in _manifest:
        description += f"{_manifest['timestamp']} "
    if "ref" in _manifest:
        description += f"[{_manifest['ref']}] "
    if "pipeline" in _manifest:
        description += f"pipeline {_manifest['pipeline']} "
    if "job" in _manifest:
        description += f"job {_manifest['job']} "

    return description.strip()
