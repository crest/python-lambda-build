python-lambda-build
===================

This repository produces a Docker image for use with Gitlab CI pipelines
that builds Python-based AWS Lambda packages.

Conventions
-----------

* The source repository must be organized with source packages in 
  `src/main/python` and test sources in `src/test/python`. Tests are
  executed using _pytest_ and must adhere to the appropriate 
  conventions.
* The root of the source repository must contain either 
  a `requirements.txt` file (as produced by `pip freeze`) or a
  `Pipfile` or `Pipfile.lock` (as produced by `pipenv`). These
  requirements are installed for the build, but are _not_ included
  in the Lambda artifacts produced by the build. See below for
  the mechanism to include additional external dependencies in a 
  Lambda artifact.
* Given a Lambda named `foo`, the `src/main/python` directory
  must contain `foo_lambda.py` and a package directory named `foo`.
  By default each such `foo` found in `src/main/python` is packaged 
  in a zip file as a Lambda artifact. Optionally, you can override
  the `LAMBDA_PACKAGES` environment variable to explicitly list each
  such `foo` to be packaged as a Lambda.
* Additional Python packages from `src/main/python` can be included
  in the zip file for one or more of the Lambdas to produced as 
  artifacts. To include additional source package in to each Lambda, 
  specify the package names in the `ADDITIONAL_PACKAGES` environment
  variable. To include additional packages with the Lambda named `foo`,
  specify the package names in the `ADD_foo` environmental variable.
* External dependencies can be included with a Lambda named `foo` by
  specifying the pip requirements in the `DEPS_foo` environment variable.
  **Note that requirements specified in `requirements.txt` or `Pipfile`
  are installed only at build time and are _not_ included in any Lambda
  artifact**.

Examples
--------

Build Lambda artifacts for each `*_lambda.py` module included in 
`src/main/python`:
```
docker container run --rm -v /my/src:/build -ti summit/python-lambda-build build.sh
```

Explicitly specify Lambda artifacts to build. Each must have a corresponding 
`*_lambda.py` in `src/main/python`:
```
docker container run --rm -v /my/src:/build -ti summit/python-lambda-build LAMBDA_PACKAGES="foo bar baz" build.sh
```

Add a `shared` package from `src/main/python` to all Lambda artifacts:
```
docker container run --rm -v /my/src:/build -ti summit/python-lambda-build ADDITIONAL_PACKAGES=shared build.sh
```

Add packages `model` and `dynamo` package from `src/main/python` to Lambda 
artifact `foo`:
```
docker container run --rm -v /my/src:/build -ti summit/python-lambda-build ADD_foo="model dynamo" build.sh
```

Add external dependency `isodate==0.6.0` to Lambda artifact `foo`:
```
docker container run --rm -v /my/src:/build -ti summit/python-lambda-build DEPS_foo="isodate==0.6.0" build.sh
```
