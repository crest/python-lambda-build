ARG PY_VERSION=3.13
FROM public.ecr.aws/lambda/python:${PY_VERSION}
ARG PKG_MANAGER=dnf

RUN ${PKG_MANAGER} update -y && \
    ${PKG_MANAGER} install -y gcc zip tar && \
    ${PKG_MANAGER} clean all && \
    rm -rf /var/cache/yum

COPY manifest/ /usr/local/etc/manifest
COPY build.sh /usr/local/bin/build.sh
RUN chmod +x /usr/local/bin/build.sh

WORKDIR /build
ENTRYPOINT []
CMD ["/usr/local/bin/build.sh"]
