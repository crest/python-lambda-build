#!/bin/bash
set -e

PIP=pip3
TARGET=target
ARTIFACTS=artifacts
PYTHON_SOURCES=src/main/python
PYTHON_TEST_SOURCES=src/test/python
MANIFEST_SOURCES=/usr/local/etc/manifest
MANIFEST_FILE=manifest.txt

# PIP INDEX URL -- override by setting PYPI_URL
PIP_INDEX_URL=${PYPI_URL:-https://pypi.org/simple}

# names of python packages containing lambdas
#LAMBDA_PACKAGES=

# names of source packages to be included with each lambda
#ADDITIONAL_PACKAGES="addl-package another-addl-package"

# names of source packages to be included with specific lambdas
#ADD_some_dependent="some-addl-package some-other-addl-package"

# additional external dependencies for all lambdas
#DEPENDENCIES="addl-dep another-addl-dep"

# additional external dependencies for specific lambdas
#DEPS_some_dependent="some-dependency some-other-dependency"

if [ -z "$LAMBDA_PACKAGES" ]; then
  for module in $PYTHON_SOURCES/*_lambda.py; do
    package=$(basename $module _lambda.py)
    if [ -z "$LAMBDA_PACKAGES" ]; then
      LAMBDA_PACKAGES=$package
    else
      LAMBDA_PACKAGES="$LAMBDA_PACKAGES $package"
    fi
  done
fi

echo "Building lambdas [$LAMBDA_PACKAGES]"

if [ -z "$LAMBDA_PACKAGES" ]; then 
  echo "No modules named *_lambda.py found in $PYTHON_SOURCES" >&2
  echo "Use LAMBDA_PACKAGES to specify them explicitly" >&2
  exit 1
fi
# create build target directory
if [ -d $TARGET ]; then
  rm -fr $TARGET
fi
mkdir $TARGET
mkdir $TARGET/$ARTIFACTS

# create build paths
build_paths=$(mktemp)
for package in $LAMBDA_PACKAGES $ADDITIONAL_PACKAGES; do
  tests_path=${PYTHON_TEST_SOURCES}/${package}
  if [ -d "$tests_path" ]; then
    echo $tests_path >> $build_paths
  fi
done

echo "Installing build requirements"
$PIP install --upgrade pip
python -m venv venv
source venv/bin/activate

if [ ! -f requirements.txt -a ! -f Pipfile ]; then
  echo "Requires one of requirements.txt or Pipfile" >&2
  exit 1
fi

if [ ! -f requirements.txt -o Pipfile.lock -nt requirements.txt -o Pipfile -nt requirements.txt ]; then
  $PIP install --upgrade pipenv
  pipenv install --dev
else
  $PIP install -r requirements.txt
fi

PYTHONPATH=$PYTHON_SOURCES xargs python -m pytest < $build_paths
deactivate

# create manifest to identify this build
manifest_src=$MANIFEST_SOURCES/$MANIFEST_FILE
manifest_dest=$(mktemp)
sed "s/@NAME@/${CI_PROJECT_NAME:-unknown}/; \
     s/@BUILD@/${CI_COMMIT_SHORT_SHA:-unknown}/; \
     s/@TIMESTAMP@/${CI_COMMIT_TIMESTAMP:-$(date +'%Y-%m-%d %H:%M:%S %Z')}/; \
     s/@REF@/${CI_COMMIT_REF_NAME:-unknown}/; \
     s/@PIPELINE@/${CI_PIPELINE_ID:-unknown}/; \
     s/@JOB@/${CI_JOB_ID:-unknown}/;" \
     < $manifest_src > $manifest_dest
echo "================================ manifest ======================================"
cat $manifest_dest
echo "================================================================================"

# zip up the Lambda packages
echo "Creating lambda packages [$LAMBDA_PACKAGES]"
for package in $LAMBDA_PACKAGES; do
  if [ -f "$PYTHON_SOURCES/${package}_lambda.py" ]; then
    (cd $PYTHON_SOURCES && zip -qr9 - ${package}_lambda.py ${package}/) > $TARGET/$ARTIFACTS/${package}.zip
  else
    (cd $PYTHON_SOURCES && zip -qr9 - ${package}/) > $TARGET/$ARTIFACTS/${package}.zip
  fi
done


echo "Adding manifest to all lambda packages"
mkdir $TARGET/manifest
tar -C $MANIFEST_SOURCES -cf - . | tar -C $TARGET/manifest -xf -
cat $manifest_dest > $TARGET/manifest/$MANIFEST_FILE
(
  cd $TARGET
  for lambda_package in $LAMBDA_PACKAGES; do
    zip -qgr9 $ARTIFACTS/${lambda_package}.zip manifest/
  done
)

# add additional packages to all lambda packages
if [ -n "$ADDITIONAL_PACKAGES" ]; then
  echo "Adding [$ADDITIONAL_PACKAGES] to all lambda packages"
  (
    cd $PYTHON_SOURCES
    for package in $ADDITIONAL_PACKAGES; do
      for lambda_package in $LAMBDA_PACKAGES; do
        zip -qgr9 ../../../$TARGET/$ARTIFACTS/${lambda_package}.zip $package/
      done
    done
  )
fi

# add additional packages to specific lambda packages
(
  cd $PYTHON_SOURCES
  for package in $LAMBDA_PACKAGES; do
    eval deps=\$ADD_$package
    if [ -n "$deps" ]; then
      echo "Adding [$deps] to lambda package $package"
      for addl_pkg in $deps; do
        zip -qgr9 ../../../$TARGET/$ARTIFACTS/${package}.zip $addl_pkg/
      done
    fi
  done
)

# add additional packages to all lambda packages
if [ -n "$DEPENDENCIES" ]; then
  (
    for package in $LAMBDA_PACKAGES; do
      echo "Adding [$DEPENDENCIES] to lambda package $package"
      eval $PIP install --index-url ${PIP_INDEX_URL} --quiet --target $TARGET/${package}-deps $DEPENDENCIES
      cd $TARGET/${package}-deps
      zip -qgr9 ../$ARTIFACTS/${package}.zip .
      cd ../..
    done
  )
fi

# add additional external dependencies to specific packages
for package in $LAMBDA_PACKAGES; do
  eval deps=\$DEPS_$package
  if [ -n "$deps" ]; then
    echo "Adding [$deps] to lambda package $package"
    eval $PIP install --index-url ${PIP_INDEX_URL} --quiet --target $TARGET/${package}-deps $deps
    cd $TARGET/${package}-deps
    zip -qgr9 ../$ARTIFACTS/${package}.zip .
    cd ../..
  fi
done

echo "Build completed successfully"
echo "Artifacts:"
for f in $(ls $TARGET/$ARTIFACTS/*); do
  echo "  $(basename $f)"
done

